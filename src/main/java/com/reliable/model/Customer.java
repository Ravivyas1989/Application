package com.reliable.model;

import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.boot.model.source.spi.SubclassEntitySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Component
@Entity
@Table(name = "customer")
@SuppressWarnings("serial")
public class Customer {

	public Customer(Long id, String username, String password) throws NoSuchAlgorithmException {
		this.id = id;
		this.username = username;
		String salt = generateSalt();
		this.salt = salt;
		String saltandpassword = methodSaltandpassword(salt, password);
		this.saltandpassword = saltandpassword;
	}

	public Customer(String username, String password) {
		this.username = username;
		String salt = generateSalt();
		this.salt = salt;
		String saltandpassword = methodSaltandpassword(salt, password);
		this.saltandpassword = saltandpassword;
	}

	public Customer() {
	}

	@Id
	@GeneratedValue
	@Column(name = "id", length = 11)
	private Long id;

	@Column(name = "username")
	String username;

	String password;

	@Column(name = "salt")
	String salt;

	@Column(name = "saltandpassword")
	String saltandpassword;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSaltandpassword() {
		return saltandpassword;
	}

	public void setSaltandpassword(String saltandpassword) {
		this.saltandpassword = saltandpassword;
	}

	public static String generateSalt() {
		Random r = new SecureRandom();
		byte[] saltBytes = new byte[32];
		r.nextBytes(saltBytes);
		return Base64.getEncoder().encodeToString(saltBytes);
	}

	public String methodSaltandpassword(String mySalt, String password) {
		String saltandpassword = null;
		try {
			System.out.println(" in methodSaltandpassword( password is " + password);
			saltandpassword = hashPasswordAndSalt(password + mySalt);
			System.out.println("saltandpassword " + saltandpassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return saltandpassword;
	}

	private String hashPasswordAndSalt(String string) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(string.getBytes());
		byte[] mdArray = md.digest();
		StringBuilder sb = new StringBuilder(mdArray.length * 2);
		for (byte b : mdArray) {
			int v = b & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString();
	}

}
